package com.example.eventplanner_team24.services;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.eventplanner_team24.FragmentTransition;
import com.example.eventplanner_team24.R;
import com.example.eventplanner_team24.databinding.FragmentProductsPageBinding;
import com.example.eventplanner_team24.model.Category;
import com.example.eventplanner_team24.model.Event;
import com.example.eventplanner_team24.model.Service;
import com.example.eventplanner_team24.model.SubCategory;
import com.example.eventplanner_team24.products.AddProductFragment;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link ServicesPageFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ServicesPageFragment extends Fragment {

    public static ArrayList<Service> services = new ArrayList<Service>();
    private FragmentProductsPageBinding binding;

    public ServicesPageFragment() {
        // Required empty public constructor
    }

    public static ServicesPageFragment newInstance(String param1, String param2) {
        ServicesPageFragment fragment = new ServicesPageFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        FragmentActivity currentActivity = getActivity();

        binding = FragmentProductsPageBinding.inflate(inflater, container, false);
        View root = binding.getRoot();

        FragmentTransition.to(ServicesFragment.newInstance(services), currentActivity, true, R.id.list_layout_products);

        prepareServicesList(services);

        FloatingActionButton addButton = binding.floatingActionButtonProducts;
        addButton.setVisibility(View.VISIBLE);

        Button btnFilters = binding.btnFilters;
        btnFilters.setVisibility(View.VISIBLE);
        btnFilters.setOnClickListener(v -> {
            Log.i("ShopApp", "Bottom Sheet Dialog");
            BottomSheetDialog bottomSheetDialog = new BottomSheetDialog(requireActivity());
            View dialogView = getLayoutInflater().inflate(R.layout.product_filter, null);
            bottomSheetDialog.setContentView(dialogView);
            bottomSheetDialog.show();
        });

        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addButton.setVisibility(View.GONE);
                FragmentTransition.to(AddServiceFragment.newInstance(), currentActivity, true, R.id.list_layout_products);
                btnFilters.setVisibility(View.GONE);
            }
        });
        return root;
    }


    private void prepareServicesList(ArrayList<Service> services){
        Category category1 = new Category(1L,"Foto i video","Profesionalno fotografisanje i snimanje događaja.");
        SubCategory subCategory1= new SubCategory(1L,"Fotografije i albumi","Profesionalno fotografisanje događaja (ceremonije, prijemi, portreti).\n" +
                "Angažovanje fotografa za celodnevno praćenje događaja. Specijalizovane foto\n" +
                "sesije (veridba, pre-wedding, post-wedding)",category1,SubCategory.Status.PRODUCT);
        ArrayList<SubCategory> sub= new ArrayList<>();
        sub.add(subCategory1);
        ArrayList<Integer> gallery1 = new ArrayList<>();
        gallery1.add(R.drawable.album);
        gallery1.add(R.drawable.camera);
        ArrayList<Event> events=new ArrayList<Event>();

        events.add(new Event(1L,"svadbe","svadba svadba svadba",sub));
        ArrayList<String> eventType1 = new ArrayList<>();
        eventType1.add("vencanje");
        eventType1.add("1 rodjendan");
        eventType1.add("krstenje");
        ArrayList<String> persons1= new ArrayList<>();
        persons1.add("Pera");
        persons1.add("Igor");
        ArrayList<String> persons2= new ArrayList<>();
        persons2.add("Pera");
        services.add(new Service(1L,category1,subCategory1,"Snimanje dronom",
                "Ovo je snimanje iz vazduha sa dronom",gallery1,
                "ne radimo praznicma.",3000.00,2,
                "okolina Novog Sada",0.00,persons1,events,
                365,2,false,
                true,true));
        services.add(new Service(2L,category1,subCategory1,"Snimanje kamerom 4k",
                "Ovo je snimanje u 4k rezoluciji",gallery1,
                "/",5000.00,2,
                "okolina Novog Sada",0.00,persons1,events,
                365,2,false,
                true,true));
        services.add(new Service(3L,category1,subCategory1,"Fotografisanje dogadjaja",
                "- Fotografisanje događaja sa najboljim kamerama.",gallery1,
                "ne radimo praznicma.",3000.00,2,
                "okolina Novog Sada",0.00,persons1,events,
                365,2,false,
                true,true));


    }

    public void onDestroyView() {
        super.onDestroyView();
        services=new ArrayList<Service>();
    }
}