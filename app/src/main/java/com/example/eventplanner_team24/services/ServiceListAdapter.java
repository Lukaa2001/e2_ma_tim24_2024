package com.example.eventplanner_team24.services;


import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentActivity;

import com.example.eventplanner_team24.FragmentTransition;
import com.example.eventplanner_team24.R;
import com.example.eventplanner_team24.model.Service;

import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicBoolean;

public class ServiceListAdapter extends ArrayAdapter<Service> {
    private ArrayList<Service> aService;
    private FragmentActivity mContext;

    public ServiceListAdapter(FragmentActivity context, ArrayList<Service> services){
        super(context, R.layout.service_list_item);
        mContext = context;
        aService = services;
    }

    @Override
    public int getCount() {
        return aService.size();
    }

    @Nullable
    @Override
    public Service getItem(int position) {
        return aService.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        Service service = getItem(position);
        if(convertView == null){
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.service_list_item,
                    parent, false);
        }
        LinearLayout serviceItem = convertView.findViewById(R.id.service_list_item);
        TextView serviceName = convertView.findViewById(R.id.service_name);
        Button viewButton = convertView.findViewById(R.id.view_service_button);
        Button addToFavorites = convertView.findViewById(R.id.service_to_favorites_button);

        ImageView imageView = convertView.findViewById(R.id.service_image);

        if(service != null){
            serviceName.setText(service.getName());
            imageView.setImageResource(service.getGallery().get(0));
        }

        viewButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentTransition.to(SingleServiceFragment.newInstance(service),mContext,true,R.id.list_layout_products);
            }
        });

        final AtomicBoolean isFavorite = new AtomicBoolean(false);

        addToFavorites.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Toggle the state of the button
                isFavorite.set(!isFavorite.get());

                // Change the button text based on the state
                if (isFavorite.get()) {
                    addToFavorites.setBackgroundColor(0xE57373);
                    // Add logic to handle adding to favorites
                } else {
                    addToFavorites.setText("Favorite");
                    // Add logic to handle removing from favorites
                }
            }
        });

        return convertView;
    }
}
