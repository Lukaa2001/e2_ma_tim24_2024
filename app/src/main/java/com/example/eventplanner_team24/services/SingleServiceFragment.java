package com.example.eventplanner_team24.services;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;

import com.example.eventplanner_team24.R;
import com.example.eventplanner_team24.databinding.FragmentSingleServiceBinding;
import com.example.eventplanner_team24.model.Event;
import com.example.eventplanner_team24.model.Service;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link SingleServiceFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class SingleServiceFragment extends Fragment {

    private FragmentSingleServiceBinding binding;
    private Service mService;


    private static final String ARG_PRODUCT = "product";

    public SingleServiceFragment() {
        // Required empty public constructor
    }

    public static SingleServiceFragment newInstance(Service service) {
        SingleServiceFragment fragment = new SingleServiceFragment();
        Bundle args = new Bundle();
        args.putParcelable(ARG_PRODUCT, service);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mService = getArguments().getParcelable(ARG_PRODUCT);
        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        binding = FragmentSingleServiceBinding.inflate(inflater, container, false);
        View root = binding.getRoot();
        EditText serviceName = binding.serviceNameEdit;
        EditText serviceDescription = binding.serviceDescriptionEdit;
        EditText serviceCategory = binding.serviceCategoryEdit;
        EditText serviceSubcategory = binding.serviceSubcategoryEdit;
        Spinner serviceSubCategorySpinner=binding.serviceSubcategorySpinner;
        EditText servicePrice = binding.servicePriceEdit;
        EditText serviceDiscount = binding.serviceDiscountEdit;
        EditText servicePriceWithDiscount = binding.servicePriceWithDiscountEdit;
        ImageView imageView=binding.serviceImageSingle;
        EditText serviceEventType = binding.serviceEventTypeEdit;
        CheckBox serviceAvailable = binding.serviceAvailableCheck;
        CheckBox serviceVisible = binding.serviceVisibleEdit;
        EditText serviceLocation = binding.serviceLocationEdit;
        EditText serviceHours = binding.serviceHoursEdit;
        EditText serviceSpecificity = binding.serviceSpecificityEdit;
        EditText servicePersons = binding.servicePersonsEdit;
        CheckBox serviceAutomaticAcceptance = binding.serviceAutomaticAcceptanceEdit;
        Spinner eventTypeSpinner=binding.eventTypeSpinner;
        Button addEventType=binding.addEventtype;
        Button deleteEventType=binding.deleteEventtype;
        Spinner personSpinner=binding.personsSpinner;
        Button addPerson=binding.addPerson;
        Button deletePerson=binding.deletePerson;

        EditText serviceDeadlineReservation = binding.serviceDeadlineReservationEdit;
        EditText serviceDeadlineCancel = binding.serviceDeadlineCancelEdit;

        Button addImageButton=binding.addImageService;
        Button deleteImageButton=binding.deleteImageService;


        String[] subcategoryOptions = {"Foto i video", "Fotografisanje","Foto i album"};
        ArrayAdapter<String> adapter = new ArrayAdapter<>(requireContext(), android.R.layout.simple_spinner_item, subcategoryOptions);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        serviceSubCategorySpinner.setAdapter(adapter);

        String[] eventTypeList = {"svadbe", "vencanje", "krstenje"};
        ArrayAdapter<String> adapterEventType = new ArrayAdapter<>(requireContext(), android.R.layout.simple_spinner_item, eventTypeList);
        adapterEventType.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        eventTypeSpinner.setAdapter(adapterEventType);

        String[] personList = {"Marko", "Djordje", "Nikola","Pera","Jova"};
        ArrayAdapter<String> adapterPerson = new ArrayAdapter<>(requireContext(), android.R.layout.simple_spinner_item, personList);
        adapterPerson.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        personSpinner.setAdapter(adapterPerson);




        if (mService != null) {
            serviceName.setText(mService.getName());
            serviceDescription.setText(mService.getDescription());
            serviceCategory.setText(mService.getCategory().getName());
            serviceSubcategory.setText(mService.getSubcategory().getName());
            servicePrice.setText(String.valueOf(mService.getPricePerHour()));
            serviceDiscount.setText(String.valueOf(mService.getDiscount()));
            servicePriceWithDiscount.setText(String.valueOf(mService.getPriceTotal()));
            serviceSpecificity.setText(mService.getSpecificity());
            serviceHours.setText(String.valueOf(mService.getHours()));
            serviceLocation.setText(mService.getLocation());
            serviceDeadlineReservation.setText(String.valueOf(mService.getDeadlineForReservation()));
            serviceDeadlineCancel.setText(String.valueOf(mService.getDeadlineForCancel()));

// For ArrayList attributes (gallery, eventTypes, persons)
           /* if (mService.getGallery() != null) {
                StringBuilder galleryText = new StringBuilder();
                for (String image : mService.getGallery()) {
                    galleryText.append(image).append("\n");
                }
                serviceGallery.setText(galleryText.toString());
            }*/

            if (mService.getEventTypes() != null) {
                StringBuilder eventTypeText = new StringBuilder();
                for (Event type : mService.getEventTypes()) {
                    eventTypeText.append(type).append("\n");
                }
                serviceEventType.setText(eventTypeText.toString());
            }

            if (mService.getPersons() != null) {
                StringBuilder personsText = new StringBuilder();
                for (String person : mService.getPersons()) {
                    personsText.append(person).append("\n");
                }
                // Assuming you have a TextView to display persons
                // TextView personsTextView = findViewById(R.id.personsTextView);
                servicePersons.setText(personsText.toString());
            }

            serviceAvailable.setChecked(mService.isAvailable());
            serviceVisible.setChecked(mService.isVisible());
            serviceAutomaticAcceptance.setChecked(mService.isAutomaticAcceptance());

        }

        imageView.setOnClickListener(new View.OnClickListener() {
            int position=0;
            @Override
            public void onClick(View v) {
                // Increment the position to change to the next image
                position++;
                if (position >= mService.getGallery().size()) {
                    // Reset position to 0 when it reaches the end of the list
                    position = 0;
                }
                // Change the image in the ImageView
                imageView.setImageResource(mService.getGallery().get(position));
            }
        });



        return root;
    }
}