package com.example.eventplanner_team24.event_manager;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.example.eventplanner_team24.FragmentTransition;
import com.example.eventplanner_team24.R;
import com.example.eventplanner_team24.databinding.FragmentEventManagerBinding;
import com.example.eventplanner_team24.event_manager.adapters.EventAdapter;
import com.example.eventplanner_team24.model.Event;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link EventManager#newInstance} factory method to
 * create an instance of this fragment.
 */
public class EventManager extends Fragment {

    private FragmentEventManagerBinding binding;
    private ArrayList<Event> events;
    public EventManager() {
        // Required empty public constructor
    }

    public static EventManager newInstance(String param1, String param2) {
        EventManager fragment = new EventManager();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentEventManagerBinding.inflate(inflater,container,false);
        View root = binding.getRoot();
        prepareEvents();
        FragmentTransition.to(EventTypeListFragment.newInstance(events), getActivity(), false, R.id.event_manager_container);
        return root;
    }

    private void prepareEvents() {
        events = new ArrayList<>();

        for (int i = 1; i <= 5; i++) {
            Event event = new Event();
            event.setId((long) i);
            event.setName("Event Type " + i);
            event.setDescription("Description for Event Type " + i);
            event.setSuggestedSubCategories(new ArrayList<>());
            events.add(event);
        }
    }
}