package com.example.eventplanner_team24.event_manager.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentActivity;

import com.example.eventplanner_team24.FragmentTransition;
import com.example.eventplanner_team24.R;
import com.example.eventplanner_team24.event_manager.EventTypeOverview;
import com.example.eventplanner_team24.model.Event;

import java.util.ArrayList;

public class EventAdapter extends ArrayAdapter<Event> {
    private ArrayList<Event> mEvents;
    private FragmentActivity mContext;

    public EventAdapter(FragmentActivity context, ArrayList<Event> events){
        super(context, R.layout.event_card, events);
        mContext = context;
        mEvents= events;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View listItem = convertView;
        if(listItem == null) {
            listItem = LayoutInflater.from(mContext).inflate(R.layout.event_card, parent, false);
        }

        Event currentEvent = mEvents.get(position);

        LinearLayout eventCard = listItem.findViewById(R.id.your_linear_layout);
        eventCard.setOnClickListener((new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentTransition.to(EventTypeOverview.newInstance(currentEvent), mContext,
                        true, R.id.event_manager_container);
            }
        }));

        TextView eventName = listItem.findViewById(R.id.event_name_title);
        eventName.setText(currentEvent.getName());

        TextView eventDescription = listItem.findViewById(R.id.event_description);
        eventDescription.setText(currentEvent.getDescription());

        return listItem;
    }
}
