package com.example.eventplanner_team24.event_manager;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.example.eventplanner_team24.R;
import com.example.eventplanner_team24.databinding.FragmentAddEventBinding;
import com.example.eventplanner_team24.databinding.FragmentEventTypeOverviewBinding;
import com.example.eventplanner_team24.model.Event;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link AddEventFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class AddEventFragment extends Fragment {

    private static final String ARG_PARAM = "param";
    private Event event;

    private FragmentAddEventBinding binding;

    public AddEventFragment() {
        // Required empty public constructor
    }

    public static AddEventFragment newInstance(Event event) {
        AddEventFragment fragment = new AddEventFragment();
        Bundle args = new Bundle();
        args.putParcelable(ARG_PARAM, event);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            event = getArguments().getParcelable(ARG_PARAM);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentAddEventBinding.inflate(inflater,container,false);
        View root = binding.getRoot();
        EditText nametextView = binding.eventNameEditText;
        EditText description = binding.eventDescriptionEditText;
        EditText subCategories = binding.subCategoriesEditText;
        if(event != null){
            nametextView.setText(event.getName());
            description.setText(event.getDescription());
            subCategories.setText("Sub Category1, Sub Category2");
        }
        return root;
    }
}