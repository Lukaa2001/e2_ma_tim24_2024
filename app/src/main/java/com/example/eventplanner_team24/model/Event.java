package com.example.eventplanner_team24.model;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;

import java.util.ArrayList;

public class Event implements Parcelable {

    private Long id;
    private String name;
    private String description;
    private ArrayList<SubCategory> suggestedSubCategories;

    public Event() {
    }

    public Event(Long id, String name, String description, ArrayList<SubCategory> suggestedSubCategories) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.suggestedSubCategories = suggestedSubCategories;
    }

    protected Event(Parcel in){
        id = in.readLong();
        name = in.readString();
        description = in.readString();
        suggestedSubCategories = in.createTypedArrayList(SubCategory.CREATOR);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public ArrayList<SubCategory> getSuggestedSubCategories() {
        return suggestedSubCategories;
    }

    public void setSuggestedSubCategories(ArrayList<SubCategory> suggestedSubCategories) {
        this.suggestedSubCategories = suggestedSubCategories;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(id);
        dest.writeString(name);
        dest.writeString(description);
        dest.writeTypedList(suggestedSubCategories);
    }

    public static final Creator<Event> CREATOR = new Creator<Event>() {
        @Override
        public Event createFromParcel(Parcel in) {
            return new Event(in);
        }

        @Override
        public Event[] newArray(int size) {
            return new Event[size];
        }
    };

    @NonNull
    @Override
    public String toString() {
        return name; // Assuming 'name' is the field containing the name of the event
    }
}
