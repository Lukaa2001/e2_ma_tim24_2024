package com.example.eventplanner_team24.model;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;

import java.util.ArrayList;

public class Service implements Parcelable {
    private Long id;
    private Category category;
    private SubCategory subcategory;
    private String name;
    private String description;
    private ArrayList<Integer> gallery;
    private String specificity;
    private double pricePerHour;
    private double priceTotal;
    private double hours;
    private String location;
    private double discount;
    private ArrayList<String> persons;
    private ArrayList<Event> eventTypes;
    private int deadlineForReservation;//days before deadline for reservation
    private int deadlineForCancel;//days before deadline for canceling
    private boolean isAutomaticAcceptance;
    private boolean isAvailable;
    private boolean isVisible;


    protected Service(Parcel in) {
        if (in.readByte() == 0) {
            id = null;
        } else {
            id = in.readLong();
        }
        category = in.readParcelable(Category.class.getClassLoader());
        subcategory = in.readParcelable(SubCategory.class.getClassLoader());
        name = in.readString();
        description = in.readString();
        specificity = in.readString();
        pricePerHour = in.readDouble();
        priceTotal = in.readDouble();
        hours = in.readDouble();
        location = in.readString();
        discount = in.readDouble();
        persons = in.createStringArrayList();
        eventTypes = in.createTypedArrayList(Event.CREATOR);
        deadlineForReservation = in.readInt();
        deadlineForCancel = in.readInt();
        isAutomaticAcceptance = in.readByte() != 0;
        isAvailable = in.readByte() != 0;
        isVisible = in.readByte() != 0;
        isDeleted = in.readByte() != 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (id == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeLong(id);
        }
        dest.writeParcelable(category, flags);
        dest.writeParcelable(subcategory, flags);
        dest.writeString(name);
        dest.writeString(description);
        dest.writeString(specificity);
        dest.writeDouble(pricePerHour);
        dest.writeDouble(priceTotal);
        dest.writeDouble(hours);
        dest.writeString(location);
        dest.writeDouble(discount);
        dest.writeStringList(persons);
        dest.writeTypedList(eventTypes);
        dest.writeInt(deadlineForReservation);
        dest.writeInt(deadlineForCancel);
        dest.writeByte((byte) (isAutomaticAcceptance ? 1 : 0));
        dest.writeByte((byte) (isAvailable ? 1 : 0));
        dest.writeByte((byte) (isVisible ? 1 : 0));
        dest.writeByte((byte) (isDeleted ? 1 : 0));
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Service> CREATOR = new Creator<Service>() {
        @Override
        public Service createFromParcel(Parcel in) {
            return new Service(in);
        }

        @Override
        public Service[] newArray(int size) {
            return new Service[size];
        }
    };

    public boolean isVisible() {
        return isVisible;
    }

    public void setVisible(boolean visible) {
        isVisible = visible;
    }

    private boolean isDeleted;

    public Service(){

    }
    public Service(Long id, Category category, SubCategory subcategory, String name, String description, ArrayList<Integer> gallery,
                   String specificity, double pricePerHour, double hours, String location, double discount,
                   ArrayList<String> persons, ArrayList<Event> eventTypes, int deadlineForReservation,
                   int deadlineForCancel, boolean isAutomaticAcceptance, boolean isAvailable,boolean isVisible) {
        this.id = id;
        this.category = category;
        this.subcategory = subcategory;
        this.name = name;
        this.description = description;
        this.gallery = gallery;
        this.specificity = specificity;
        this.pricePerHour = pricePerHour;
        this.hours = hours;
        this.location = location;
        this.discount = discount;
        this.persons = persons;
        this.eventTypes = eventTypes;
        this.deadlineForReservation = deadlineForReservation;
        this.deadlineForCancel = deadlineForCancel;
        this.isAutomaticAcceptance = isAutomaticAcceptance;
        this.isAvailable = isAvailable;
        this.isVisible= isVisible;
        this.isDeleted = false;
        calculateTotalPrice();
    }





    private void calculateTotalPrice() {
        // Calculate total price based on pricePerHour, hours, and discount
        priceTotal = (pricePerHour * hours) * (1 - (discount / 100));
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public SubCategory getSubcategory() {
        return subcategory;
    }

    public void setSubcategory(SubCategory subcategory) {
        this.subcategory = subcategory;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public ArrayList<Integer> getGallery() {
        return gallery;
    }

    public void setGallery(ArrayList<Integer> gallery) {
        this.gallery = gallery;
    }

    public String getSpecificity() {
        return specificity;
    }

    public void setSpecificity(String specificity) {
        this.specificity = specificity;
    }

    public double getPricePerHour() {
        return pricePerHour;
    }

    public void setPricePerHour(double pricePerHour) {
        this.pricePerHour = pricePerHour;
    }

    public double getPriceTotal() {
        return priceTotal;
    }

    public void setPriceTotal(double priceTotal) {
        this.priceTotal = priceTotal;
    }

    public double getHours() {
        return hours;
    }

    public void setHours(double hours) {
        this.hours = hours;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public double getDiscount() {
        return discount;
    }

    public void setDiscount(double discount) {
        this.discount = discount;
    }

    public ArrayList<String> getPersons() {
        return persons;
    }

    public void setPersons(ArrayList<String> persons) {
        this.persons = persons;
    }

    public ArrayList<Event> getEventTypes() {
        return eventTypes;
    }

    public void setEventTypes(ArrayList<Event> eventTypes) {
        this.eventTypes = eventTypes;
    }

    public int getDeadlineForReservation() {
        return deadlineForReservation;
    }

    public void setDeadlineForReservation(int deadlineForReservation) {
        this.deadlineForReservation = deadlineForReservation;
    }

    public int getDeadlineForCancel() {
        return deadlineForCancel;
    }

    public void setDeadlineForCancel(int deadlineForCancel) {
        this.deadlineForCancel = deadlineForCancel;
    }

    public boolean isAutomaticAcceptance() {
        return isAutomaticAcceptance;
    }

    public void setAutomaticAcceptance(boolean automaticAcceptance) {
        isAutomaticAcceptance = automaticAcceptance;
    }

    public boolean isAvailable() {
        return isAvailable;
    }

    public void setAvailable(boolean available) {
        isAvailable = available;
    }

    public boolean isDeleted() {
        return isDeleted;
    }

    public void setDeleted(boolean deleted) {
        isDeleted = deleted;
    }

    @Override
    public String toString() {
        return name;
    }


}
