package com.example.eventplanner_team24.authentication.interfaces;

public interface IUserTypeListener {
    void onUserTypeSelected(String userType);

}

