package com.example.eventplanner_team24.authentication.interfaces;

import com.example.eventplanner_team24.model.User;

public interface IUserCreatedListener {

    void onUserCreated(User user);
}
