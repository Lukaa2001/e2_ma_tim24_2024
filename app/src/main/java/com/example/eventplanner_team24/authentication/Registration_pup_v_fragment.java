package com.example.eventplanner_team24.authentication;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.eventplanner_team24.R;
import com.google.firebase.auth.FirebaseAuth;
import java.util.ArrayList;
import java.util.List;
public class Registration_pup_v_fragment extends Fragment {

    private FirebaseAuth auth;
    private EditText companyName, companyEmail, companyAddress, companyPhoneNumber, aboutCompany;
    private Button registerButton;
    private Spinner serviceCategorySpinner;

    public Registration_pup_v_fragment() {
        // Required empty public constructor
    }


    public static Registration_pup_v_fragment newInstance(String param1, String param2) {
        Registration_pup_v_fragment fragment = new Registration_pup_v_fragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        auth = FirebaseAuth.getInstance();
        View rootView = inflater.inflate(R.layout.fragment_registration_pup_v_fragment, container, false);

        //companyName, companyEmail, companyAddress, companyPhoneNumber, aboutCompany;
        companyName = rootView.findViewById(R.id.companyNameEditText);
        companyEmail = rootView.findViewById(R.id.companyEmailEditText);
        companyAddress = rootView.findViewById(R.id.companyAddressEditText);
        companyPhoneNumber = rootView.findViewById(R.id.companyPhoneNumberEditText);
        aboutCompany = rootView.findViewById(R.id.companyDescriptionEditText);

        registerButton = (Button)rootView.findViewById(R.id.registerCompanyButton);
/*

        List<String> categories = new ArrayList<>();
        categories.add("Category 1");
        categories.add("Category 2");
        categories.add("Category 3");

        ArrayAdapter<String> adapter = new ArrayAdapter<>(requireContext(), android.R.layout.simple_spinner_item, categories);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        serviceCategorySpinner.setAdapter(adapter);

*/
        registerButton.setOnClickListener(v -> {
            Toast.makeText(getContext(), "Registration successful", Toast.LENGTH_SHORT).show();
        });


        // Inflate the layout for this fragment
        return rootView;
    }
}