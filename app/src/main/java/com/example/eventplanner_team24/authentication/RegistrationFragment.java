package com.example.eventplanner_team24.authentication;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.example.eventplanner_team24.R;
import com.example.eventplanner_team24.authentication.interfaces.IUserCreatedListener;
import com.example.eventplanner_team24.authentication.interfaces.IUserTypeListener;
import com.example.eventplanner_team24.model.User;
import com.google.firebase.auth.FirebaseAuth;

public class RegistrationFragment extends Fragment {

    private FirebaseAuth auth;
    private EditText email, password, confirmPassword, firstName, lastName, address, phoneNumber;
    private Button registerButton;

    private IUserTypeListener userTypeListener;
    private IUserCreatedListener userCreationListener;

    public RegistrationFragment() {
        // Required empty public constructor
    }


    public static RegistrationFragment newInstance(String param1, String param2) {
        RegistrationFragment fragment = new RegistrationFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }




    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);



    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        auth = FirebaseAuth.getInstance();
        View rootView = inflater.inflate(R.layout.fragment_registration, container, false);

        email = rootView.findViewById(R.id.emailEditText);
        password = rootView.findViewById(R.id.passwordEditText);
        confirmPassword = rootView.findViewById(R.id.confirmPasswordEditText);
        firstName = rootView.findViewById(R.id.firstNameEditText);
        lastName = rootView.findViewById(R.id.lastNameEditText);
        address = rootView.findViewById(R.id.addressEditText);
        phoneNumber = rootView.findViewById(R.id.phoneNumberEditText);

        RadioGroup radioGroup = (RadioGroup)rootView.findViewById(R.id.userTypeRadioGroup);

        registerButton = (Button)rootView.findViewById(R.id.registerButton);

        radioGroup.setOnCheckedChangeListener(((group, checkedId) -> {
            if(checkedId == R.id.userTypeRadioButton1){
                registerButton.setText("Register");
            }else if (checkedId == R.id.userTypeRadioButton2){
                registerButton.setText("Next step");
            }
        }));


        registerButton.setOnClickListener(v -> {
            String emailString = email.getText().toString();
            String passwordString = password.getText().toString();
            String confirmPasswordString = confirmPassword.getText().toString();
            String firstNameString = firstName.getText().toString();
            String lastNameString = lastName.getText().toString();
            String addressString = address.getText().toString();
            String phoneNumberString = phoneNumber.getText().toString();

            if (TextUtils.isEmpty(emailString)) {
                email.setError("Email cannot be empty!");
                return;
            }
            if (TextUtils.isEmpty(passwordString)) {
                password.setError("Password cannot be empty!");
                return;
            }
            if (TextUtils.isEmpty(confirmPasswordString)) {
                confirmPassword.setError("Confirm Password cannot be empty!");
                return;
            }
            if (TextUtils.isEmpty(firstNameString)) {
                firstName.setError("First Name cannot be empty!");
                return;
            }
            if (TextUtils.isEmpty(lastNameString)) {
                lastName.setError("Last Name cannot be empty!");
                return;
            }
            if (TextUtils.isEmpty(addressString)) {
                address.setError("Address cannot be empty!");
                return;
            }
            if (TextUtils.isEmpty(phoneNumberString)) {
                phoneNumber.setError("Phone Number cannot be empty!");
                return;
            }

            if (!checkPasswordsMatch(passwordString, confirmPasswordString)) {
                confirmPassword.setError("Passwords do not match!");
                return;
            }

            int selectedRadioButtonId = radioGroup.getCheckedRadioButtonId();
            if (selectedRadioButtonId == R.id.userTypeRadioButton1) {
                auth.createUserWithEmailAndPassword(emailString, passwordString).addOnCompleteListener(
                        task -> {
                            if(task.isSuccessful()){
                                Toast.makeText(getContext(), "Registration successful", Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(getContext(), "Registration failed: " + task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }
                );
            } else if (selectedRadioButtonId == R.id.userTypeRadioButton2) {
                userTypeListener.onUserTypeSelected("PUP-V");



                User newUser = new User(firstNameString, lastNameString, emailString,
                        passwordString, addressString, phoneNumberString);
                userCreationListener.onUserCreated(newUser);
            } else {

                Toast.makeText(getContext(), "Please select a user type", Toast.LENGTH_SHORT).show();
            }
        });

        return rootView;
    }

    private boolean checkPasswordsMatch(String password, String confirmPassword){
        return password.equals(confirmPassword);
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        try {
            userTypeListener = (IUserTypeListener) context;
            userCreationListener = (IUserCreatedListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString() + " must implement UserTypeListener");
        }
    }

}