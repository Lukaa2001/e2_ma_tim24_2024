package com.example.eventplanner_team24.category_manager.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentActivity;

import com.example.eventplanner_team24.FragmentTransition;
import com.example.eventplanner_team24.R;
import com.example.eventplanner_team24.category_manager.FragmentCreateCategory;
import com.example.eventplanner_team24.category_manager.FragmentCreateSubCategory;
import com.example.eventplanner_team24.event_manager.EventTypeOverview;
import com.example.eventplanner_team24.model.Category;
import com.example.eventplanner_team24.model.Event;

import java.util.ArrayList;

public class CategoryAdapter extends ArrayAdapter<Category> {
    private ArrayList<Category> mCategories;
    private FragmentActivity mContext;

    public CategoryAdapter(FragmentActivity context, ArrayList<Category> categories){
        super(context, R.layout.category_card, categories);
        mContext = context;
        mCategories= categories;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View listItem = convertView;
        if(listItem == null) {
            listItem = LayoutInflater.from(mContext).inflate(R.layout.category_card, parent, false);
        }

        Category currentCategory = mCategories.get(position);

        TextView categoryName = listItem.findViewById(R.id.category_title);
        categoryName.setText(currentCategory.getName());

        TextView categoryDescription = listItem.findViewById(R.id.category_description);
        categoryDescription.setText(currentCategory.getDescription());

        ImageView editIcon = listItem.findViewById(R.id.edit_icon);
        editIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentTransition.to(FragmentCreateCategory.newInstance(currentCategory), mContext, false, R.id.category_manager_container);
            }
        });


        return listItem;
    }
}