package com.example.eventplanner_team24.category_manager;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.example.eventplanner_team24.R;
import com.example.eventplanner_team24.databinding.FragmentAddEventBinding;
import com.example.eventplanner_team24.databinding.FragmentCreateCategoryBinding;
import com.example.eventplanner_team24.event_manager.AddEventFragment;
import com.example.eventplanner_team24.model.Category;
import com.example.eventplanner_team24.model.Event;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link FragmentCreateCategory#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FragmentCreateCategory extends Fragment {

    private static final String ARG_PARAM = "param";
    private Category category;
    private FragmentCreateCategoryBinding binding;

    public FragmentCreateCategory() {
        // Required empty public constructor
    }

    public static FragmentCreateCategory newInstance(Category category) {
        FragmentCreateCategory fragment = new FragmentCreateCategory();
        Bundle args = new Bundle();
        args.putParcelable(ARG_PARAM, category);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            category = getArguments().getParcelable(ARG_PARAM);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentCreateCategoryBinding.inflate(inflater,container,false);
        View root = binding.getRoot();
        EditText nametextView = binding.categoryName;
        EditText description = binding.categoryDescription;
        if(category != null){
            nametextView.setText(category.getName());
            description.setText(category.getDescription());
        }
        return root;
    }
}