package com.example.eventplanner_team24.category_manager;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.eventplanner_team24.FragmentTransition;
import com.example.eventplanner_team24.R;
import com.example.eventplanner_team24.databinding.FragmentCategoryManagerBinding;
import com.example.eventplanner_team24.databinding.FragmentEventManagerBinding;
import com.example.eventplanner_team24.event_manager.EventManager;
import com.example.eventplanner_team24.event_manager.EventTypeListFragment;
import com.example.eventplanner_team24.model.Category;
import com.example.eventplanner_team24.model.SubCategory;
import com.google.android.material.tabs.TabLayout;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link CategoryManagerFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class CategoryManagerFragment extends Fragment {

    private FragmentCategoryManagerBinding binding;
    private ArrayList<Category> categories;
    private ArrayList<SubCategory> subCategories;
    private static final String ARG_PARAM1 = "param1";

    public CategoryManagerFragment() {
        // Required empty public constructor
    }


    public static CategoryManagerFragment newInstance(String param1, String param2) {
        CategoryManagerFragment fragment = new CategoryManagerFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentCategoryManagerBinding.inflate(inflater,container,false);
        View root = binding.getRoot();
        prepareCategories();
        FragmentTransition.to(FragmentCategoryList.newInstance(categories), getActivity(), false, R.id.category_manager_container);


        binding.tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                int position = tab.getPosition();
                switch (position) {
                    case 0:
                        FragmentTransition.to(FragmentCategoryList.newInstance(categories), getActivity(), false, R.id.category_manager_container);
                        break;
                    case 1:
                        FragmentTransition.to(FragmentSubCategoryList.newInstance(subCategories), getActivity(), false, R.id.category_manager_container);
                        // Perform fragment transition for sub categories
                        break;
                    case 2:
                        // Third tab selected (Suggestions)
                        // Perform fragment transition for suggestions
                        break;
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                // Unused, but required method
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                // Unused, but required method
            }
        });

        binding.addCategoryButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int selectedTabIndex = binding.tabLayout.getSelectedTabPosition();
                switch (selectedTabIndex) {
                    case 0:
                        binding.searchCategoriesLayout.setVisibility(View.GONE);
                        FragmentTransition.to(FragmentCreateCategory.newInstance(null), getActivity(), false, R.id.category_manager_container);
                        break;
                    case 1:
                        binding.searchCategoriesLayout.setVisibility(View.GONE);
                        FragmentTransition.to(FragmentCreateSubCategory.newInstance(null), getActivity(), false, R.id.category_manager_container);
                        break;
                    case 2:

                        break;
                }
            }
        });


        return root;
    }

    @Override
    public void onStop() {
        super.onStop();
        binding.categoryManagerLayout.setVisibility(View.VISIBLE);
    }


    private void prepareCategories() {
        categories = new ArrayList<>();
        subCategories = new ArrayList<>();
        for (int i = 1; i <= 5; i++) {
            Category category = new Category();
            category.setId((long) i);
            category.setName("Category " + i);
            category.setDescription("Description for Category " + i);

            // Create subcategories for each category
            ArrayList<SubCategory> subCategories1 = new ArrayList<>();
            for (int j = 1; j <= 5; j++) {
                SubCategory subCategory = new SubCategory();
                subCategory.setId((long) (i * 10 + j));
                subCategory.setName("Subcategory " + j + " of Category " + i);
                subCategory.setDescription("Description for Subcategory " + j + " of Category " + i);
                subCategory.setCategory(category);
                subCategory.setStatus(SubCategory.Status.SERVICE);
                subCategories1.add(subCategory);
                subCategories.add(subCategory);
            }

            categories.add(category);
        }
    }

}