package com.example.eventplanner_team24.category_manager;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.example.eventplanner_team24.R;
import com.example.eventplanner_team24.category_manager.adapters.SubCategoryAdapter;
import com.example.eventplanner_team24.databinding.FragmentCreateCategoryBinding;
import com.example.eventplanner_team24.databinding.FragmentCreateSubCategoryBinding;
import com.example.eventplanner_team24.model.Category;
import com.example.eventplanner_team24.model.SubCategory;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link FragmentCreateSubCategory#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FragmentCreateSubCategory extends Fragment {

    private static final String ARG_PARAM = "param";
    private SubCategory subCategory;
    private FragmentCreateSubCategoryBinding binding;

    public FragmentCreateSubCategory() {
        // Required empty public constructor
    }

    public static FragmentCreateSubCategory newInstance(SubCategory subCategory) {
        FragmentCreateSubCategory fragment = new FragmentCreateSubCategory();
        Bundle args = new Bundle();
        args.putParcelable(ARG_PARAM, subCategory);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            subCategory = getArguments().getParcelable(ARG_PARAM);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentCreateSubCategoryBinding.inflate(inflater,container,false);
        View root = binding.getRoot();
        EditText nametextView = binding.edittextName;
        EditText description = binding.edittextDescription;
        if(subCategory != null){
            nametextView.setText(subCategory.getName());
            description.setText(subCategory.getDescription());
        }
        return root;
    }
}