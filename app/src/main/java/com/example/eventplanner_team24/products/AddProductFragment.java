package com.example.eventplanner_team24.products;

import android.app.AlertDialog;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;

import com.example.eventplanner_team24.R;
import com.example.eventplanner_team24.databinding.FragmentAddProductBinding;
import com.example.eventplanner_team24.model.Category;
import com.example.eventplanner_team24.model.Event;
import com.example.eventplanner_team24.model.SubCategory;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link AddProductFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class AddProductFragment extends Fragment {

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private FragmentAddProductBinding binding;
    private ArrayList<Integer> selectedImages = new ArrayList<>();

    private String mParam1;
    private String mParam2;
    public AddProductFragment() {
        // Required empty public constructor
    }

    public static AddProductFragment newInstance() {
        AddProductFragment fragment = new AddProductFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        binding = FragmentAddProductBinding.inflate(inflater, container, false);
        View root = binding.getRoot();

        EditText productName = binding.productName;
        Button addImage = binding.addImageButton;
        Spinner categorySpinner=binding.categoryBtnProduct;
        Spinner subCategorySpinner=binding.subcategoryBtnProduct;
        Spinner eventTypeSpinner=binding.eventTypeBtnProduct;

        ArrayList<Integer> images = new ArrayList<>();
        images.add(R.drawable.album);
        images.add(R.drawable.camera);
        images.add(R.drawable.photographing);

        ArrayList<Category> categories = new ArrayList<>();
        Category fotoCategory=new Category(1L,"Cvetni aranžmani","cap");
        Category restaurantCategory=new Category(2L,"Ugostiteljstvo","cap");
        categories.add(fotoCategory);
        categories.add(restaurantCategory);


        ArrayAdapter<Category> adapterCategory = new ArrayAdapter<>(requireContext(), android.R.layout.simple_spinner_item, categories);
        adapterCategory.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        categorySpinner.setAdapter(adapterCategory);

        ArrayList<SubCategory> subCategories=new ArrayList<>();
        SubCategory fotoalbum=new SubCategory(1L,"Svadbeni buketi","lol",fotoCategory,SubCategory.Status.PRODUCT);
        SubCategory ketering= new SubCategory(1L,"ketering","cap",restaurantCategory,SubCategory.Status.SERVICE);
        subCategories.add(fotoalbum);
        subCategories.add(ketering);

        ArrayAdapter<SubCategory> adapterSubCategory = new ArrayAdapter<>(requireContext(), android.R.layout.simple_spinner_item, subCategories);
        adapterSubCategory.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        subCategorySpinner.setAdapter(adapterSubCategory);

        ArrayList<Event> events=new ArrayList<>();
        Event svadba=new Event(1L,"Krstenje","krstenje",subCategories);
        Event rodjendan= new Event(2L,"Proslava diplomskog","cap",subCategories);
        events.add(svadba);
        events.add(rodjendan);

        ArrayAdapter<Event> adapterEvent = new ArrayAdapter<>(requireContext(), android.R.layout.simple_spinner_item, events);
        adapterEvent.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        eventTypeSpinner.setAdapter(adapterEvent);


        addImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showImagesDialog(images);
            }
        });

        categorySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Category selectedCategory = (Category) parent.getSelectedItem();
                ArrayList<SubCategory> filteredSubCategories = filterSubCategoriesByCategory(selectedCategory);
                ArrayAdapter<SubCategory> adapterSubCategory = new ArrayAdapter<>(requireContext(), android.R.layout.simple_spinner_item, filteredSubCategories);
                adapterSubCategory.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                subCategorySpinner.setAdapter(adapterSubCategory);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // Handle when nothing is selected
            }

            private ArrayList<SubCategory> filterSubCategoriesByCategory(Category category) {
                ArrayList<SubCategory> filteredSubCategories = new ArrayList<>();
                for (SubCategory subCategory : subCategories) {
                    if (subCategory.getCategory().equals(category)) {
                        filteredSubCategories.add(subCategory);
                    }
                }
                filteredSubCategories.add(new SubCategory(-1L, "Add New Subcategory", "", category, SubCategory.Status.PRODUCT));
                return filteredSubCategories;
            }
        });




        return root;
    }

    private void showImagesDialog(ArrayList<Integer> images) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = requireActivity().getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.dialog_add_image, null);
        builder.setView(dialogView);

        // Initialize Spinner
        Spinner imageSpinner = dialogView.findViewById(R.id.image_spinner);
        ArrayAdapter<Integer> adapter = new ArrayAdapter<>(requireContext(), android.R.layout.simple_spinner_item, images);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        imageSpinner.setAdapter(adapter);


        builder.setPositiveButton("Add", (dialog, which) -> {
            Integer selectedImage = (Integer) imageSpinner.getSelectedItem();
            if (selectedImage != null) {
                addSelectedImage(selectedImage);
            }
        });

        builder.setNegativeButton("Cancel", (dialog, which) -> dialog.dismiss());

        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    private void addSelectedImage(Integer selectedImage) {
        selectedImages.add(selectedImage); // Dodajemo novu subkategoriju u listu

        // Pronalazimo ListView
        ListView selectedImagesListView = requireView().findViewById(R.id.images_listview);

        // Ako je ListView bio nevidljiv, sada ga prikazujemo
        selectedImagesListView.setVisibility(View.VISIBLE);

        // Ako već nemamo adapter, kreiramo novi i postavljamo ga na ListView
        if (selectedImagesListView.getAdapter() == null) {
            ArrayAdapter<Integer> selectedImagesAdapter = new ArrayAdapter<>(requireContext(), android.R.layout.simple_list_item_1, selectedImages);
            selectedImagesListView.setAdapter(selectedImagesAdapter);
        } else { // Ako već imamo adapter, samo ga ažuriramo
            ArrayAdapter<Integer> selectedImagesAdapter = (ArrayAdapter<Integer>) selectedImagesListView.getAdapter();
            selectedImagesAdapter.notifyDataSetChanged();
        }
    }
}