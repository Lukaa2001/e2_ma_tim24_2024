package com.example.eventplanner_team24.products;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.eventplanner_team24.FragmentTransition;
import com.example.eventplanner_team24.R;
import com.example.eventplanner_team24.databinding.FragmentProductsPageBinding;
import com.example.eventplanner_team24.model.Category;
import com.example.eventplanner_team24.model.Event;
import com.example.eventplanner_team24.model.Product;
import com.example.eventplanner_team24.model.SubCategory;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link ProductsPageFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ProductsPageFragment extends Fragment {
    public static ArrayList<Product> products = new ArrayList<Product>();
    private FragmentProductsPageBinding binding;
    public ProductsPageFragment() {
        // Required empty public constructor
    }

    public static ProductsPageFragment newInstance() {
        ProductsPageFragment fragment = new ProductsPageFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        FragmentActivity currentActivity = getActivity();
        binding = FragmentProductsPageBinding.inflate(inflater,container,false);
        View root = binding.getRoot();

        prepareProductsList(products);
        FragmentTransition.to(ProductsFragment.newInstance(products),currentActivity,true,R.id.list_layout_products);

        FloatingActionButton addButton = binding.floatingActionButtonProducts;
        addButton.setVisibility(View.VISIBLE);


        Button btnFilters = binding.btnFilters;
        btnFilters.setVisibility(View.VISIBLE);
        btnFilters.setOnClickListener(v -> {
            Log.i("ShopApp", "Bottom Sheet Dialog");
            BottomSheetDialog bottomSheetDialog = new BottomSheetDialog(requireActivity());
            View dialogView = getLayoutInflater().inflate(R.layout.product_filter, null);
            bottomSheetDialog.setContentView(dialogView);
            bottomSheetDialog.show();
        });

        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addButton.setVisibility(View.GONE);
                FragmentTransition.to(AddProductFragment.newInstance(), currentActivity, true, R.id.list_layout_products);
                btnFilters.setVisibility(View.GONE);
            }
        });


        return root;
    }

    private void prepareProductsList(ArrayList<Product> products){

        Category category1 = new Category(2L, "Cvetni aranžmani", "Profesionalno aranžiranje cvetnih aranžmana za sve prilike.");
        SubCategory subCategory1 = new SubCategory(2L, "Svadbeni buketi", "Elegantni buketi za venčanja u različitim stilovima.", category1, SubCategory.Status.PRODUCT);


        ArrayList<SubCategory> sub= new ArrayList<>();
        sub.add(subCategory1);
        ArrayList<Integer> gallery1 = new ArrayList<>();
        gallery1.add(R.drawable.album);
        gallery1.add(R.drawable.camera);
        ArrayList<Event> events=new ArrayList<Event>();

        events.add(new Event(1L,"svadbe","svadba svadba svadba",sub));
        ArrayList<String> eventType1 = new ArrayList<>();
        eventType1.add("svadbe");
        eventType1.add("rodjendani");
        eventType1.add("krstenja");
        eventType1.add("sajmovi");


        products.add(new Product(3L, category1, subCategory1, "Profesionalno fotografisanje", "Profesionalno fotografisanje događaja", 3000.00, 0.00, gallery1, events, true, true));
        products.add(new Product(4L, category1, subCategory1, "Video snimanje", "Video snimanje događaja", 4000.00, 0.00, gallery1, events, true, true));
    }

    public void onDestroyView() {
        super.onDestroyView();
        products=new ArrayList<Product>();
    }
}


