package com.example.eventplanner_team24.products;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;

import com.example.eventplanner_team24.R;
import com.example.eventplanner_team24.databinding.FragmentSingleProductBinding;
import com.example.eventplanner_team24.model.Event;
import com.example.eventplanner_team24.model.Product;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link SingleProductFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class SingleProductFragment extends Fragment {
    private static final String ARG_PRODUCT = "product";
    private FragmentSingleProductBinding binding;
    private Product mProduct;
    public static SingleProductFragment newInstance(Product product) {
        SingleProductFragment fragment = new SingleProductFragment();
        Bundle args = new Bundle();
        args.putParcelable(ARG_PRODUCT, product);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mProduct = getArguments().getParcelable(ARG_PRODUCT);
        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        binding = FragmentSingleProductBinding.inflate(inflater, container, false);
        View root = binding.getRoot();
        EditText productName = binding.productNameEdit;
        EditText productDescription = binding.productDescriptionEdit;
        EditText productCategory = binding.productCategoryEdit;
        EditText productSubcategory = binding.productSubcategoryEdit;
        Spinner productSubCategorySpinner=binding.productSubcategorySpinner;
        EditText productPrice = binding.productPriceEdit;
        EditText productDiscount = binding.productDiscountEdit;
        EditText productPriceWithDiscount = binding.productPriceWithDiscountEdit;
        EditText productEventType = binding.productEventTypeEdit;
        CheckBox productAvailable = binding.productAvailableCheck;
        CheckBox productVisible = binding.productVisibleEdit;
        ImageView imageView = binding.productImageSingle;
        Button addImageButton=binding.addImage;
        Button deleteImageButton=binding.deleteImage;
        Spinner eventTypeSpinner=binding.eventTypeSpinner;
        Button addEventType=binding.addEventtype;
        Button deleteEventType=binding.deleteEventtype;

        String[] subcategoryOptions = {"Foto i video", "Fotografisanje","Foto i album"};
        ArrayAdapter<String> adapter = new ArrayAdapter<>(requireContext(), android.R.layout.simple_spinner_item, subcategoryOptions);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        productSubCategorySpinner.setAdapter(adapter);


        String[] eventTypeList = {"svadbe", "vencanje", "krstenje"};
        ArrayAdapter<String> adapterEventType = new ArrayAdapter<>(requireContext(), android.R.layout.simple_spinner_item, eventTypeList);
        adapterEventType.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        eventTypeSpinner.setAdapter(adapterEventType);

        if (mProduct != null) {
            productName.setText(mProduct.getName());
            productDescription.setText(mProduct.getDescription());
            productCategory.setText(mProduct.getCategory().getName());
            productSubcategory.setText(mProduct.getSubcategory().getName());
            productPrice.setText(String.valueOf(mProduct.getPrice()));
            productDiscount.setText(String.valueOf(mProduct.getDiscount()));
            productPriceWithDiscount.setText(String.valueOf(mProduct.getPriceWithDiscount()));
            imageView.setImageResource(mProduct.getGallery().get(0));
            // For ArrayList attributes (gallery, eventType)
            /*if (mProduct.getGallery() != null) {

                for (int image : mProduct.getGallery()) {
                    galleryText.append(image).append("\n");
                }
                productGallery.setText(galleryText.toString());
            }*/

            if (mProduct.getEventType() != null) {
                StringBuilder eventTypeText = new StringBuilder();
                for (Event type : mProduct.getEventType()) {
                    eventTypeText.append(type).append("\n");
                }
                productEventType.setText(eventTypeText.toString());
            }

            productAvailable.setChecked(mProduct.isAvailable());
            productVisible.setChecked(mProduct.isVisible());
        }






        imageView.setOnClickListener(new View.OnClickListener() {
            int position=0;
            @Override
            public void onClick(View v) {
                // Increment the position to change to the next image
                position++;
                if (position >= mProduct.getGallery().size()) {
                    // Reset position to 0 when it reaches the end of the list
                    position = 0;
                }
                // Change the image in the ImageView
                imageView.setImageResource(mProduct.getGallery().get(position));
            }
        });

        productPrice.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // Not needed
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // Not needed
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.toString().isEmpty()) {
                    try {
                        // Attempt to parse the input as a decimal number
                        Double.parseDouble(s.toString());
                        productPrice.setText(s.toString());
                    } catch (NumberFormatException e) {
                        productPrice.setText("");
                    }
                }
            }
        });

// Method to validate the input as a decimal number




        return root;
    }
}