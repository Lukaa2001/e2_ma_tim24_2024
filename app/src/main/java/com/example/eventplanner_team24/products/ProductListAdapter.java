package com.example.eventplanner_team24.products;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.eventplanner_team24.FragmentTransition;
import com.example.eventplanner_team24.R;
import com.example.eventplanner_team24.model.Product;
import com.example.eventplanner_team24.products.placeholder.PlaceholderContent.PlaceholderItem;
import com.example.eventplanner_team24.databinding.FragmentProductsBinding;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;


public class ProductListAdapter extends ArrayAdapter<Product> {
    private ArrayList<Product> aProduct;
    private FragmentActivity mContext;

    public ProductListAdapter(FragmentActivity context, ArrayList<Product> products){
        super(context, R.layout.product_list_item);
        mContext = context;
        aProduct = products;
    }

    @Override
    public int getCount() {
        return aProduct.size();
    }

    @Nullable
    @Override
    public Product getItem(int position) {
        return aProduct.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        Product product = getItem(position);
        if(convertView == null){
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.product_list_item,
                    parent, false);
        }
        LinearLayout productItem = convertView.findViewById(R.id.product_list_item);
        TextView productName = convertView.findViewById(R.id.product_name);
        Button viewButton = convertView.findViewById(R.id.view_product_button);
        Button addToFavorites = convertView.findViewById(R.id.product_to_favorites_button);
//        ImageView imageView = convertView.findViewById(R.id.product_image);

        if(product != null){
            productName.setText(product.getName());
//            imageView.setImageResource(product.getGallery().get(0));
        }

        viewButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentTransition.to(SingleProductFragment.newInstance(product),mContext,true,R.id.list_layout_products);
            }
        });

        final AtomicBoolean isFavorite = new AtomicBoolean(false);
        addToFavorites.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Toggle the state of the button
                isFavorite.set(!isFavorite.get());

                // Change the button text based on the state
                if (isFavorite.get()) {
                    addToFavorites.setBackgroundColor(0xE57373);
                    // Add logic to handle adding to favorites
                } else {
                    addToFavorites.setText("Favorite");
                    // Add logic to handle removing from favorites
                }
            }
        });

        return convertView;
    }
}
